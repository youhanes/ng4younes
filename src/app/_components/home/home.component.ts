import { Component, OnInit } from '@angular/core';
import { NewsService } from 'src/app/_services/news.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  news = [];
  isLoading = true;

  currentItem = {};

  constructor(private newsService: NewsService) { }

  ngOnInit() {
    this.allNews();
  }

  // get all Data
  allNews() {
    this.newsService.getAllNews().subscribe((res: any[]) => {
      this.news = res;
      this.isLoading = false;
      console.log(res);
    });
  }

  // get the current Titel after click
  readMore(res) {
    this.currentItem = res;
    console.log('clicked', this.currentItem );
  }

}
